<?php

require_once 'url.php';

function gemtext_xhtml($writer, $socket, $url=NULL) {
	$in_pre = FALSE;
	$in_list = FALSE;
	while (($line = fgets($socket))) {
		if (preg_match('/^(#{1,3}) (.*)$/', $line, $m)) {
			$writer->startElement('h'.strlen($m[1]));
			$writer->text($m[2]);
			$writer->endElement();
		} else if (preg_match('/^=>\s*(\S*)\s*(.*)$/', $line, $m)) {
			$writer->startElement('p');
			$writer->startElement('a');
			$href = $url ? getAbsoluteURL($m[1], $url) : $m[1];
			$writer->writeAttribute('href', $href);
			$writer->text($m[2]);
			$writer->endElement();
			$writer->endElement();
		} else if (preg_match('/^> (.*)$/', $line, $m)) {
			$writer->startElement('blockquote');
			$writer->text($m[1]);
			$writer->endElement();
		} else if (preg_match('/^```/', $line, $m)) {
			if ($in_pre) {
				$writer->endElement();
				$in_pre = FALSE;
			} else {
				$writer->startElement('pre');
				$in_pre = TRUE;
			}
		} else if (preg_match('/^\* (.*)$/', $line, $m)) {
			if (!$in_list) {
				$writer->startElement('ul');
				$in_list = TRUE;
			}
			$writer->startElement('li');
			$writer->text($m[1]);
			$writer->endElement();
		} else if ($in_pre) {
			$writer->text($line);
		} else if (preg_match('/^\s*$/', $line)) {
			$writer->writeElement('br');
		} else {
			if ($in_list) {
				$writer->endElement();
				$in_list = FALSE;
			}
			$writer->startElement('p');
			$writer->text($line);
			$writer->endElement();
		}
	}
	if ($in_pre) $writer->endElement();
	if ($in_list) $writer->endElement();
}

?>
