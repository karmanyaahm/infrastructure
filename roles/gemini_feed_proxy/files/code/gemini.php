<?php

function fwrite_all($fp, $str) {
	$len = strlen($str);
	while ($len > 0) {
		$wrote = fwrite($fp, $str);
		if ($wrote === FALSE) return FALSE;
		$len -= $wrote;
		$str = substr($str, $len);
	}
}

function gemini_connect($url, &$sock) {
	$parts = (object)parse_url($url);
	if ($parts->scheme !== 'gemini') return 'Scheme must be gemini';
	$host = $parts->host;
	$port = isset($parts->port) ? $parts->port : 1965;
	$path = $parts->path;
	if (isset($parts->query)) $path .= '?'.$parts->query;
	// TODO: TOFU?
	$ctx = stream_context_create([
		'ssl' => [
			'verify_peer' => FALSE,
			'verify_peer_name' => TRUE,
			'allow_self_signed' => TRUE,
		]
	]);
	$sock = stream_socket_client("ssl://$host:$port", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $ctx);
	if (!$sock) die("Unable to create socket: $errstr ($errno)");
	$wrote = fwrite_all($sock, $url."\r\n");
	if ($wrote === FALSE) {
		fclose($sock);
		return 'Unable to send request';
	}
}

function gemini_resp_parse($sock, &$status, &$meta) {
	$line = fgets($sock);
	if ($line === FALSE) return 'Unable to read status line';
	$line = trim($line);
	$i = strpos($line, ' ');
	$status = substr($line, 0, $i);
	$meta = substr($line, $i+1);
}

function gemini_fetch($url, &$status, &$meta, &$sock) {
	$err = gemini_connect($url, $sock);
	if ($err) return $err;
	return gemini_resp_parse($sock, $status, $meta);
}

function gemini_cached_fetch($url, $cachedir, $updated, &$status, &$meta, &$sock) {
	$parts = (object)parse_url($url);
	if ($parts->scheme !== 'gemini') return 'Scheme must be gemini';
	$filename = $cachedir
		.'/'.$parts->host.(isset($parts->port) ? ':'.$parts->port : '')
		.$parts->path.(isset($parts->query) ? '?'.$parts->query : '');
	if (preg_match('|/$|', $filename)) $filename .= '.index';
	if (file_exists($filename)) {
		$mtime = filemtime($filename);
		if ($mtime >= $updated) {
			// return cached file
			$sock = fopen($filename, 'r');
			if ($sock == NULL) return 'Unable to open file';
			return gemini_resp_parse($sock, $status, $meta);
		}
	}
	$err = gemini_connect($url, $sock);
	if ($err) return $err;
	$tmpdir = $cachedir.'/tmp';
	$tmp_filename = @tempnam($tmpdir, 'gemini');
	if ($tmp_filename === FALSE) return 'Unable to create temp file';
	$fp = fopen($tmp_filename, 'w+');
	if ($fp === FALSE) {
		fclose($sock);
		return 'Unable to save file';
	}
	// save resource to file
	while (($data = fread($sock, 8192))) {
		$wrote = fwrite_all($fp, $data);
		if ($wrote === FALSE) {
			fclose($sock);
			fclose($fp);
			return 'Unable to save file';
		}
	}
	fclose($sock);
	// rewind file and give it to caller
	$sock = $fp;
	fseek($fp, 0, SEEK_SET);
	// save temp file for later use
	$dir = dirname($filename);
	if (!is_dir($dir)) mkdir($dir, 0755, TRUE);
	touch($tmp_filename, $updated);
	rename($tmp_filename, $filename);
	return gemini_resp_parse($sock, $status, $meta);
}

?>
