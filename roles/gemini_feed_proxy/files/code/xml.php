<?php
require_once 'gemini.php';
require_once 'gemtext.php';

$homedir = $_SERVER['HOME'];
$cachedir = $homedir.'/.cache/gemini';

class XMLTransformer {
	private $url;
	private $parser;
	private $writer;
	private $stack;
	private $is_feed;
	private $entry_id;
	private $entry_href;
	private $entry_has_content;
	private $entry_updated;
	private $entry_created;

	function __construct($output_uri, $url) {
		$this->url = $url;
		$this->parser = xml_parser_create();
		$this->writer = new \XMLWriter();
		$this->writer->openUri($output_uri);

		$this->stack = array();

		xml_set_object($this->parser, $this);
		xml_set_element_handler($this->parser, 'tag_open', 'tag_close');
		xml_set_character_data_handler($this->parser, 'cdata');
		xml_set_processing_instruction_handler($this->parser, 'processing_instr');
		xml_set_default_handler($this->parser, 'default_handler');
		xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, FALSE);
	}

	function __destruct() {
		xml_parser_free($this->parser);
		unset($this->parser);
		$this->writer->flush();
		unset($this->writer);
	}

	function parse($data) {
		xml_parse($this->parser, $data);
	}

	function tag_open($parser, $tag, $attributes) {
		$count = array_push($this->stack, strtolower($tag));

		switch (implode(',', $this->stack)) {
		case 'feed,entry':
		case 'rss,channel,item':
			$this->entry_has_content = FALSE;
			$this->entry_id = '';
			$this->entry_href = '';
			$this->entry_updated = '';
			$this->entry_created = '';
			break;

		case 'feed,entry,content':
		case 'rss,channel,item,description':
			$this->entry_has_content = TRUE;
			break;

		case 'feed,entry,link':
			if ($attributes['rel'] === 'alternate') {
				$this->entry_href = $attributes['href'];
			}
			break;
		}

		$this->writer->startElement($tag);
		foreach ($attributes as $key => $value) {
			$this->writer->writeAttribute($key, $value);
		}
	}

	function default_handler($parser, $data) {
		$this->writer->flush();
		print $data;
	}

	function cdata($parser, $cdata) {
		$this->writer->text($cdata);
		switch (implode(',', $this->stack)) {
		case 'feed,entry,id': $this->entry_id .= $cdata; break;
		case 'feed,entry,updated': $this->entry_updated .= $cdata; break;
		case 'feed,entry,created': $this->entry_created .= $cdata; break;
		case 'rss,channel,item,link': $this->entry_href .= $cdata; break;
		case 'rss,channel,item,guid': $this->entry_id .= $cdata; break;
		case 'rss,channel,item,pubDate': $this->entry_updated .= $cdata; break;
		}
	}

	function passthru_text($sock) {
		while (($data = fread($sock, 8192))) {
			$this->writer->text($data);
		}
	}

	function add_content($href, $updated) {
		global $cachedir;
		$err = gemini_cached_fetch($href, $cachedir, $updated, $status, $meta, $resp);
		if ($err) return;
		$is_atom = $this->stack[0] !== 'rss';
		if ($is_atom) {
			$this->writer->startElement('content');
		} else {
			$this->writer->startElement('description');
		}
		$meta1 = preg_replace('/;.*/', '', $meta);
		switch ($meta1) {
		case 'text/gemini':
			if ($is_atom) {
				$this->writer->writeAttribute('type', 'xhtml');
				$this->writer->startElement('div');
				$this->writer->writeAttribute('xmlns', 'http://www.w3.org/1999/xhtml');
				$this->writer->flush();
			} else {
				$this->writer->startCdata();
			}
			gemtext_xhtml($this->writer, $resp, $this->url);
			if ($is_atom) {
				$this->writer->endAttribute();
				$this->writer->endElement();
			} else {
				$this->writer->endCdata();
			}
			break;
		case 'text/plain':
			if ($is_atom) {
				$this->writer->writeAttribute('type', 'html');
			}
			$this->writer->text('<pre>');
			$this->passthru_text($resp);
			$this->writer->text('</pre>');
			break;
		case 'text/html':
			if ($is_atom) {
				$this->writer->writeAttribute('type', 'html');
			}
			$this->passthru_text($resp);
			break;
		default:
			if ($is_atom) {
				$this->writer->writeAttribute('type', $meta);
			}
			$this->passthru_text($resp);
			break;
		}
		$this->writer->text("\n");
		fclose($resp);
		$this->writer->endElement();
	}

	function tag_close($parser, $tag) {
		switch (implode(',', $this->stack)) {
		case 'feed,entry':
		case 'rss,channel,item':
			if (!$this->entry_has_content) {
				$href = $this->entry_href ? $this->entry_href : $this->entry_id;
				$updated = strtotime($this->entry_updated ? $this->entry_updated : $this->entry_created);
				$this->add_content($href, $updated);
			}
		}
		array_pop($this->stack);
		$this->writer->endElement();
	}

	function processing_instr($parser, $target, $data) {
		$this->writer->writePi($target, $data);
	}
}

function xml($sock, $url) {
	global $cachedir;
	$data = fread($sock, 8192);
	if (!$data) return;

	// handle initial PI
	// since xml_set_processing_instruction_handler doesn't seem to do it
	if (preg_match('/^\s*(<\?.*?\?>\s*)(.*)$/s', $data, $m)) {
		print $m[1];
		$data = $m[2];
	}

	// peek to see if is feed
	if (preg_match('/^\s*<(?:feed|rss)[^>]*>/', $data)) {
		$transformer = new XMLTransformer('php://output', $url, $cachedir);
		do {
			$transformer->parse($data);
		} while (($data = fread($sock, 8192)));
	} else {
		// skip processing non-feed xml
		print $data;
		fpassthru($sock);
	}
}
?>
