<?php
$url = @$_GET['url'];
if (!$url || $url === 'gemini://') {
?>
<!doctype html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Gemini Proxy</title>
		<meta name="viewport" content="initial-scale=1.0, maximum-scale=1, user-scalable=no"/>
	</head>
	<body>
		<h3>Gemini Proxy</h3>
		<form action="" method="get">
			<input name=url placeholder=URL style="width:95%">
			<input type=hidden name=redirect value=yes>
			<input type=submit>
		</form>
	</body>
</html>
<?php
	exit;
}

if (@$_GET['redirect']) {
	header('Location: /'.$url);
	die('<a href="'.htmlspecialchars($url).'">Continue</a>');
}

require_once 'gemini.php';
require_once 'gemtext.php';
$err = gemini_fetch($url, $status, $meta, $sock);
if ($err) {
	header('Content-type: text/plain', TRUE, 400);
	die('Unable to fetch: '.$err);
}
switch ($status[0]) {
	case '2':
		switch (preg_replace('/;.*/', '', $meta)) {
		case 'text/gemini':
			header('Content-type: text/html');
			require_once 'gemtext.php';
			echo '<!doctype html>';
			echo '<html xmlns="http://www.w3.org/1999/xhtml" lang="en">';
			echo '<head>';
			echo '<meta charset="utf-8" />';
			echo '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
			// echo '<title></title>';
			echo '</head>';
			echo '<body>';
			$writer = new \XMLWriter();
			$writer->openUri('php://output');
			gemtext_xhtml($writer, $sock);
			$writer->flush();
			echo '</body>';
			echo '</html>';
			break;

		case 'text/xml':
		case 'application/xml':
		case 'application/atom+xml':
			// $meta = 'text/plain';
			header('Content-type: '.$meta);
			require_once 'xml.php';
			xml($sock, $url);
			break;

		default:
			header('Content-type: '.$meta);
			fpassthru($sock);
			break;
	}
}
fclose($sock);
?>
