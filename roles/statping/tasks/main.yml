- name: Install dependencies
  apt:
    update_cache: yes
    name:
      - ruby-sass
      - haproxy
      - cron
    state: latest

- name: Install Systemd service
  template: 
    src: statping.service
    dest: /etc/systemd/system/
    owner: root
    group: root

- name: Create Statping Files Directory
  file:
    path: "{{statping_files_directory}}"
    state: directory
    owner: root
    group: root
    mode: u=rwx,g=rx,o=rx

- name: Install Statping config
  template:
    src: statping_config.yml
    dest: "{{statping_files_directory}}/config.yml"
    mode: '0666'
    owner: root
    group: root
    force: yes

- name: Version
  command: "statping version"
  register: detected_statping_version

- name: "Read version number" 
  set_fact:
    detected_statping_version: "{{detected_statping_version.stdout.split(' ')[0]}}"

- name: Download and Unarchive statping
  unarchive:
      src: https://github.com/statping/statping/releases/download/v{{statping_version}}/statping-linux-{{statping_arch}}.tar.gz
      dest: /usr/local/bin/
      group: root
      remote_src: yes
      mode: u=rwx,g=rx,o=rx
      owner: root
  when: "detected_statping_version != statping_version"


- name: Restart service 
  systemd:
    state: restarted
    daemon_reload: yes
    name: statping
    enabled: yes

- name: Install haproxy config
  template: 
    src: haproxy.cfg
    dest: /etc/haproxy/haproxy.cfg
    owner: root
    group: haproxy

- name: Get Certbot
  include_role:
    name: geerlingguy.certbot
    public: yes
  vars:
    certbot_admin_email: "{{ur_email}}"
    certbot_create_if_missing: true
    certbot_create_method: standalone
    certbot_create_standalone_stop_services: [haproxy]
    certbot_certs:
      - domains:
          - "{{domain}}"
    certbot_auto_renew: false

- name: Create SSL Directory
  file:
    path: "/etc/ssl/{{domain}}/"
    state: directory

- name: Create Haproxy Compatible SSL Cert
  shell: "cat /etc/letsencrypt/live/{{domain}}/fullchain.pem /etc/letsencrypt/live/{{domain}}/privkey.pem | tee /etc/ssl/{{domain}}/{{domain}}.pem"
  become: yes

- name: Add cron job for certbot renewal
  cron:
    name: Certbot automatic renewal.
    job: "{{ certbot_script }} renew {{ certbot_auto_renew_options }}"
    minute: "0"
    hour: "0"
    day: "1,15"
    user: "root"

- name: Restart haproxy
  systemd:
    state: reloaded
    name: haproxy
    enabled: yes