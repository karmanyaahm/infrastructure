# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "2.18.0"
  constraints = "~> 2.0, 2.18.0"
  hashes = [
    "h1:DcHmM5XmjwrCCPqIdBRvYDrth5hjk6ldjsS94szjjJc=",
    "zh:4121da1ad26081552e3a648e94c89df96e246d50c6e307fe5eba586664691de0",
    "zh:4212865eeb42f491d3409f1b9edbb508dbc781a12144c4cb157a8057965144fb",
    "zh:4965fad90d5caf7917e0f7617d76a5d3419ca3f003e408f6e58af5e53f20b1ba",
    "zh:59d5dedb2b9c9b0a3fc5ad07fce4b1aefeaef5229dbe510e7f0f9f99bbb448aa",
    "zh:6746bfa2cfe6005b64286ccf9fcc5b25d1dc29d1448fe9b4f9acf7d3f7f05f79",
    "zh:78dd4811b35ea04f0ab11a0c7c600e8fe7f30e7645d8fc60d1d02272fa85568b",
    "zh:c7a7adf710bbf686d879825428f9ba92ec35fcb44742ffac5ea9b9538c43a19a",
    "zh:cd8827681b957a9a28cb8139414fd8430f228ff736be251c32ae26d8b146bfad",
    "zh:cf398859858618b5569b2ad3e84ee5550836b70083b1e7bcf3ba8398ff06e247",
    "zh:eb11da2096aea02c792dfe1a5e3605e711102401d03a722b8ae16223245e7f70",
    "zh:fc8d289e98dfa3e846b2c737cfdd821a6e1836a062e5453265d1dbb1e35433f1",
  ]
}

provider "registry.terraform.io/linode/linode" {
  version     = "1.14.3"
  constraints = "1.14.3"
  hashes = [
    "h1:LIbucvxGtgaE+UnyaQhq+OgbO4Gwb6JXJK2EvPKx2Lc=",
    "zh:0320a50405093d7c24df1a71711434b1ea2fbfc0ac649e824081976c2d85b196",
    "zh:10cf7455074adca77ce92213658e245fa5de7b3509e93399c85eb7efceb77d02",
    "zh:31edb6dbcd1bd3ffa473a0dce5d1af5314769387b87d41352cba8ee5825b03f5",
    "zh:4c826f301e33258939ca7761e8056c4e4929136e1a6c9e38d443dba9024ea2b8",
    "zh:98207b293a02b72dd50defdae53173234782d8828dc34c3650ad795fc5d09c01",
    "zh:c30ebf3b619aba0a66d3ebe3b014df84339ebb92e24265ff6d044b00b102d582",
    "zh:c4c32d25fd9fdc16d76f57ac8595a36397e6345882ca895c31e71b3d4896cd4a",
    "zh:d9ebd39077458524f6d8aba3b82d2c3a2c22ea85b22d6153fac1977fec91acfd",
    "zh:e1e9b306591bd160cc370614d3a5da2b37c35c4e9e75330cd2e2f929f34a0890",
    "zh:f7db01252ba1314059d9929a9f5708b6cdccf74c0a271518e88b3f93c68cdda6",
    "zh:ffb8581f1bde5621677c132df822663d55956353d052b6ead51b3e11857c10e1",
  ]
}
