terraform {
  required_providers {
    linode = {
      source  = "linode/linode"
      version = "1.14.3"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "2.18.0"
    }
  }
}

provider "linode" {
  token = var.LINODE_TOKEN
}

provider "cloudflare" {
  api_token = var.cloudflare_token
}


# LINODES
resource "linode_instance" "primary" {
  image  = "linode/debian10"
  region = "us-east"
  type   = "g6-nanode-1"

  authorized_keys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICDimzXq7m8tSibbLiiuukupSUi5JzWTIo31Xco9bb1C karmanyaahm@karmanyaahsArch"]
  root_pass       = var.linode_pass[0]

  swap_size        = 256
  backups_enabled  = true
  watchdog_enabled = true

  alerts {
    cpu            = 50
    network_in     = 5
    network_out    = 5
    transfer_quota = 40
  }

}


# DNS
data "cloudflare_zones" "malhotracc" {
  filter {
    name = "malhotra.cc"
  }
}

locals {
  servers = concat([
    { type = "A", prefix = "a.linode", target = sort(linode_instance.primary.ipv4)[0] },
    { type = "AAAA", prefix = "a.linode", target = trimsuffix(linode_instance.primary.ipv6, "/128") },
  ], var.servers_dns)

  cloudflare_id = lookup(data.cloudflare_zones.malhotracc.zones[0], "id")
}


resource "cloudflare_record" "servers" {
  zone_id = local.cloudflare_id
  ttl     = 300

  count = length(local.servers)

  name  = "${local.servers[count.index].prefix}.servers"
  type  = local.servers[count.index].type
  value = local.servers[count.index].target
}

resource "cloudflare_record" "services" {
  zone_id = local.cloudflare_id
  type    = "CNAME"
  ttl     = 300

  for_each = var.services_dns_map

  name  = each.key
  value = "${each.value}.servers.malhotra.cc"
}

resource "cloudflare_record" "text" {
  zone_id = local.cloudflare_id
  type    = "TXT"

  count = length(var.dns_text)
  name  = var.dns_text[count.index][0]
  value = var.dns_text[count.index][1]
}

resource "cloudflare_record" "fix_text" {
  zone_id = local.cloudflare_id
  type    = "TXT"

  count = length(var.fix_txt)
  name  = var.fix_txt[count.index]
  value = "dnslink=/ipfs/QmQzy3RkBZ6mMgkB7XVCabapFX83KUMXUpz3yrtqjyaktG"
  lifecycle {
    ignore_changes = [value]
  }
}

