#!/usr/bin/python3

import requests

# import netifaces #pip install netifaces
import sys

PASSWORD = "{{router_dns_password}}"
# INTERFACE = "\{\{router_dns_interface\}\}"


# def get_ipv6():
#     addresses = [
#         addr["addr"]
#         for addr in netifaces.ifaddresses(INTERFACE)[netifaces.AF_INET6]
#         if addr["addr"][:2] != "fe"
#     ]
#     for i in addresses:
#         if i[:2] == "fd":
#             return i
#     try:
#         return addresses[0]
#     except:
#         raise Exception("No Address Found")

DNS_1 = sys.argv[1]
DNS_2 = "2001:db8::1"  # an address that doesn't exist

print(f"Router config sending: {DNS_1}, {DNS_2}")

# Login start
data = {"operation": "login", "password": PASSWORD}

get = requests.post("http://router/cgi-bin/luci/;stok=/login?form=login", data=data)
stok = get.json()["data"]["stok"]
auth = get.cookies["sysauth"]

cookies = {
    "sysauth": auth,
}


# Get parameters to put into the output
data = {
    "operation": "read",
    "pppflag": "v6",
    "share": "0",
}
get = requests.post(
    f"http://router/cgi-bin/luci/;stok={stok}/admin/network?form=wan_ipv6_dynamic",
    data=data,
    cookies=cookies,
)

get = get.json()["data"]
print(
    f"""
Current Address: {get["ip6addr"]}
Current DNS Mode: {get['dns_mode']}
Current DNS Servers: {get['static_pridns']}, {get['static_snddns']}
"""
)
if get["static_pridns"] == DNS_1 and get["static_snddns"] == DNS_2:
    print("DNS is the same, exiting")
    exit()

print("Sending router addresses")

# Set the addresses
data = {
    "operation": "write",
    "ip6addr": get["ip6addr"],
    "pri_dns": get["pri_dns"],
    "snd_dns": get["snd_dns"],
    "ip_config": get["ip_config"],
    "ip_mode": get["ip_mode"],
    "dns_mode": "static",
    "static_pridns": DNS_1,
    "static_snddns": DNS_2,  # new one here old are same
}

get = requests.post(
    f"http://router/cgi-bin/luci/;stok={stok}/admin/network?form=wan_ipv6_dynamic",
    data=data,
    cookies=cookies,
)

get = get.json()
# print(get)
print("Router Config sent: " + str(get["success"]))
assert get["success"]

